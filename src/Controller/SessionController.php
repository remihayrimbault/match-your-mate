<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\Type;
use App\Entity\Utilisateur;
use App\Form\SessionType;
use App\Repository\TypeRepository;
use App\Repository\UtilisateurRepository;
use App\Repository\SessionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SessionController extends AbstractController
{
    #[Route('/session', name: 'app_session')]
    public function index(SessionRepository $sessionRepository, TypeRepository $typeRepository): Response
    {
        $sessions = $sessionRepository->findAll();
        $types = $typeRepository->findAll();
        return $this->render('session/index.html.twig', [
            'sessions'=>$sessions,
            'types' => $types,
        ]);
    }

    #[Route('/session/add', name: 'add_session')]
    public function add(TypeRepository $typeRepository, Request $request): Response
    {
        $types = $typeRepository->findAll();

        return $this->render('session/add.html.twig', [

        ]);
    }

    #[Route('/session/delete/{id}', name: 'delete_session', methods: ['GET'])]
    public function delete(Session $session): Response
    {
        $del = $this->getDoctrine()->getManager();
        $del->remove($session);
        $del->flush();

        return $this->redirectToRoute('app_session');
    }

    #[Route('/session/leave/{id}', name: 'leave_session')]
    public function leave(Session $session): Response
    {
        $user = $this->getUser();
        $session->removeUtilisateur($user);

        $em=$this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();

        return $this->redirectToRoute('app_session');
    }

    #[Route('/session/submitted', name: 'submitted_session')]
    public function submitted(TypeRepository $typeRepository, UtilisateurRepository $utilisateurRepository ): Response
    {
        if(isset($_POST)) {
            $type = $typeRepository->findOneBy(array('name' => $_POST['type']));
            $user = $utilisateurRepository->findOneBy(array('email' => $_POST['user']));
            $session = new Session($type, $user);
            $em=$this->getDoctrine()->getManager();
            $em->persist($session);
            $em->flush();
        }

        return $this->redirectToRoute('app_session');
    }

    #[Route('/session/{id}/start', name: 'start_session')]
    public function start(Session $session): Response
    {
        $session->setIsLaunched(true);

        $em=$this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();

        return $this->redirectToRoute('session', ['id' => $session->getId()]);
    }

    #[Route('/session/{id}', name: 'session', methods: ['GET', 'POST'])]
    public function show(Session $session): Response
    {
        $user = $this->getUser();
        $session->addUtilisateur($user);

        $em=$this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();

        return $this->render('session/session.html.twig', [
            'session' => $session,
        ]);
    }
}
