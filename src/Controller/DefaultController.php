<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Session;
use App\Repository\ArticleRepository;
use App\Repository\SessionRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(SessionRepository $sessionRepository, UtilisateurRepository $user): Response
    {
        $showSession = $sessionRepository->findBy(array('isLaunched' => false), limit: 2);
        $sessionsNotLaunched = $sessionRepository->findBy(array('isLaunched' => false));
        $sessionsLaunched = $sessionRepository->findBy(array('isLaunched' => true));
        $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
        $showArticle = [];
        $showArticle[0] = $articles[0];
        $showArticle[1] = $articles[1];
        return $this->render('default/index.html.twig', [
            'user' => $user->findAll(),
            'articles' => $showArticle,
            'sessions' => $showSession,
            'sessionsNotLaunched' => $sessionsNotLaunched,
            'sessionsLaunched' => $sessionsLaunched,
        ]);
    }
}
