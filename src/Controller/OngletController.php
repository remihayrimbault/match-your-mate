<?php

namespace App\Controller;

use App\Repository\SessionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OngletController extends AbstractController
{
    #[Route('/onglet', name: 'app_onglet')]
    public function index(SessionRepository $sessionRepository): Response
    {
        $sessions = $sessionRepository->findAll();
        return $this->render('includes/_onglets.html.twig', [
            'sessions' => $sessions
        ]);
    }
}
