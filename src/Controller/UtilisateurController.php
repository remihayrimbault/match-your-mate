<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\Utilisateur;
use App\Repository\SessionRepository;
use ContainerQC4JzYK\getUtilisateurService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UtilisateurController extends AbstractController
{
    #[Route('/utilisateur/{id}', name: 'app_utilisateur')]
    public function index(Utilisateur $user, SessionRepository $sessionRepository): Response
    {
        $mySession = $sessionRepository->findAll();

        $sessions = $this->getDoctrine()->getRepository(Session::class)->findAll();
        $showSession = [];
        $showSession[0] = $sessions[0];
        $showSession[1] = $sessions[1];
        return $this->render('utilisateur/index.html.twig', [
            'user' => $user,
            'sessions' => $showSession,
        ]);
    }
}
