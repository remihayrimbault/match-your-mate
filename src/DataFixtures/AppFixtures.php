<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Session;
use App\Entity\Type;
use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Type de Session \\
        $tabSessionType = array();
        $tabSessionType[0] = new Type('Ranked Duo', 2, 'duo');
        $tabSessionType[1] = new Type('Ranked Flex', 5, 'flex');
        $tabSessionType[2] = new Type('Normal Draft', 5, 'draft');
        $tabSessionType[3] = new Type('ARAM', 5, 'aram');
        $tabSessionType[4] = new Type('Custom', 10, 'custom');

        foreach ($tabSessionType as $type) {
            $manager->persist($type);
        }

        // Users \\
        $tabUsers = array();
        $tabUsers[0] = new Utilisateur('test@test.fr', ["ROLE_JOUEUR"], '$2y$13$2I/8/jqTdu1fVGCetxNC5el6HUdK4ftC/Ix6hVCTFY3pKcOoNcCJS', true, 'remtkt');
        $tabUsers[1] = new Utilisateur('admin@admin.fr', ["ROLE_JOUEUR", "ROLE_ADMIN"], '$2y$13$09TgjsCekcyavd2Ld291lOw11ce4zvT8XSDt8lW1ibidhM5BKVKwa', true, 'Gaby2K');
        $tabUsers[2] = new Utilisateur('simon@simon.fr', ["ROLE_JOUEUR"], '$2y$13$HQAF1piq9uMpJLHJQ4jEbe9wGm4aEOFRHP9yuXbQtKdKIckM2xe7y', true, 'simon');
        $tabUsers[3] = new Utilisateur('rekkles@rekkles.fr', ["ROLE_JOUEUR"], '$2y$13$fOMgGBY7zwdBiudP9XI8EuwkEXdK3d9IkEGCnbG1uXhIOTeKGRcSK', true, 'KC Rekkles');
        $tabUsers[4] = new Utilisateur('joueur1@joueur.fr', ["ROLE_JOUEUR"], '$2y$13$lLB7CrNrIy3AfN168QPWWeaX8pvYf7VtsVmOQYcAIRqzjWmkF0AOW', true, 'Hirtz');
        $tabUsers[5] = new Utilisateur('joueur2@joueur.fr', ["ROLE_JOUEUR"], '$2y$13$lLB7CrNrIy3AfN168QPWWeaX8pvYf7VtsVmOQYcAIRqzjWmkF0AOW', true, 'S1GE');
        $tabUsers[6] = new Utilisateur('joueur3@joueur.fr', ["ROLE_JOUEUR"], '$2y$13$lLB7CrNrIy3AfN168QPWWeaX8pvYf7VtsVmOQYcAIRqzjWmkF0AOW', true, 'ITW Skape');
        $tabUsers[7] = new Utilisateur('joueur4@joueur.fr', ["ROLE_JOUEUR"], '$2y$13$lLB7CrNrIy3AfN168QPWWeaX8pvYf7VtsVmOQYcAIRqzjWmkF0AOW', true, 'maxousa');
        $tabUsers[8] = new Utilisateur('joueur5@joueur.fr', ["ROLE_JOUEUR"], '$2y$13$lLB7CrNrIy3AfN168QPWWeaX8pvYf7VtsVmOQYcAIRqzjWmkF0AOW', true, 'iReezo');
        $tabUsers[9] = new Utilisateur('joueur6@joueur.fr', ["ROLE_JOUEUR"], '$2y$13$lLB7CrNrIy3AfN168QPWWeaX8pvYf7VtsVmOQYcAIRqzjWmkF0AOW', true, 'Pascal Poro');

        foreach ($tabUsers as $user) {
            $manager->persist($user);
        }

        // Articles \\
        $tabArticles = array();
        $tabArticles[0] = new Article('KCORP au sommet', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 'https://www.remihr.fr/karmine.jpeg', $tabUsers[0] );
        $tabArticles[1] = new Article('Solary déboussolé ?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',  'https://www.remihr.fr/solary.jpeg', $tabUsers[0] );
        $tabArticles[2] = new Article('Patch Note 12.3.5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',  'https://www.remihr.fr/lol.jpeg', $tabUsers[0] );
        $tabArticles[3] = new Article('Arcane et League of Legends', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',  'https://www.remihr.fr/arcane.jpeg', $tabUsers[0] );

        foreach ($tabArticles as $article) {
            $manager->persist($article);
        }

        // Sessions \\
        $tabSessions = array();
        $tabSessions[0] = new Session($tabSessionType[0], $tabUsers[0]);
        $tabSessions[1] = new Session($tabSessionType[1], $tabUsers[1]);
        $tabSessions[2] = new Session($tabSessionType[2], $tabUsers[2]);
        $tabSessions[3] = new Session($tabSessionType[4], $tabUsers[3]);
        $tabSessions[4] = new Session($tabSessionType[0], $tabUsers[4]);
        $tabSessions[5] = new Session($tabSessionType[1], $tabUsers[5]);

        $tabSessions[1]->addUtilisateur($tabUsers[0]);
        $tabSessions[1]->addUtilisateur($tabUsers[8]);
        $tabSessions[1]->addUtilisateur($tabUsers[9]);

        $tabSessions[1]->addUtilisateur($tabUsers[8]);
        $tabSessions[2]->addUtilisateur($tabUsers[7]);
        $tabSessions[2]->addUtilisateur($tabUsers[6]);

        $tabSessions[3]->addUtilisateur($tabUsers[5]);

        foreach ($tabSessions as $session) {
            $manager->persist($session);
        }

        $manager->flush();
    }
}
