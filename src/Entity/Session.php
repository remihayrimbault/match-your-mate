<?php

namespace App\Entity;

use App\Repository\SessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SessionRepository::class)]
class Session
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[ORM\Column(type: 'boolean')]
    private $isLaunched;

    #[ORM\ManyToOne(targetEntity: Type::class, inversedBy: 'sessions')]
    #[ORM\JoinColumn(nullable: false)]
    private $Type;

    #[ORM\ManyToMany(targetEntity: Utilisateur::class, mappedBy: 'SessionUser')]
    private $utilisateurs;

    #[ORM\ManyToOne(targetEntity: Utilisateur::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $idCreator;

    public function __construct($type, $idCreator)
    {
        $this->createdAt = new \DateTime('now');
        $this->isLaunched = false;
        $this->Type = $type;
        $this->idCreator = $idCreator;
        $this->utilisateurs = new ArrayCollection();
        $this->addUtilisateur($idCreator);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsLaunched(): ?bool
    {
        return $this->isLaunched;
    }

    public function setIsLaunched(bool $isLaunched): self
    {
        $this->isLaunched = $isLaunched;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->Type;
    }

    public function setType(?Type $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    /**
     * @return Collection<int, Utilisateur>
     */
    public function getUtilisateurs(): Collection
    {
        return $this->utilisateurs;
    }

    public function addUtilisateur(Utilisateur $utilisateur): self
    {
        if (!$this->utilisateurs->contains($utilisateur)) {
            $this->utilisateurs[] = $utilisateur;
            $utilisateur->addSessionUser($this);
        }

        return $this;
    }

    public function removeUtilisateur(Utilisateur $utilisateur): self
    {
        if ($this->utilisateurs->removeElement($utilisateur)) {
            $utilisateur->removeSessionUser($this);
        }

        return $this;
    }

    public function getIdCreator(): ?Utilisateur
    {
        return $this->idCreator;
    }

    public function setIdCreator(?Utilisateur $idCreator): self
    {
        $this->idCreator = $idCreator;

        return $this;
    }
}
